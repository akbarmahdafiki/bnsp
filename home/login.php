<?php
    session_start();
    session_destroy();
    include 'head.php';
?>
    <body>
        <div class="wrapper login">
            <img src="../assets/img/logo_unas.png" alt="logo unas" width="80px" height="90px">
            <h3 class="judul-login">LOGIN</h3>
            <div>
                <form method="POST" action="cek_login.php" class="form-login">
                    <div class="wrap-input">
                        <i class="fa fa-user logo-login"></i>
                        <input type="text" name="username" placeholder="Username .." required="required">
                    </div>
                    <br>
                    <div class="wrap-input">
                        <i class="fa fa-key logo-login"></i>
                        <input type="password" name="password" placeholder="Password .." required="required">
                    </div>
                    <br>
        
                    <input type="submit" class="btn btn-success btn-login col-sm-5" value="Masuk">
                    <br>
                    <br>
                    <a href="daftar_akun.php">Belum Punya Akun?</a><br>
                </form>
            </div>
	    </div>
    </body>

</html>

<?php
    if(isset($_GET['notif'])){
        if($_GET['notif']=="gagal"){
            echo "<script>alert('Username dan password tidak terdaftar !!');</script>";
        }
        elseif ($_GET['notif']=="bukan_admin") {
            echo "<script>alert('Maaf, hanya admin yang dapat mengakses halaman ini !!');</script>";
        }
        elseif ($_GET['notif']=="tidak_login") {
            echo "<script>alert('Anda harus login supaya dapat mengakses halaman ini !!');</script>";
        }
    }
?>
