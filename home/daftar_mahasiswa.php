<?php
    include '../connect.php';                
    $tampil_dosen = mysqli_query($koneksi, "SELECT nama FROM data_dosen ORDER BY 'no_id'");
    
    include 'head.php';
?>
<body>
    <div class="wrapper">
        <center>
        <h2>Pendaftaran Mahasiswa</h2><br/>
        </center>
        <form method="POST" action="daftar_mahasiswa.php" enctype="multipart/form-data">
            <table class="table table-striped">
                <tr>
                    <td><label for="nama">Masukan Nama *: </label></td>
                    <td><input class="col-sm-4" type="text" name="nama" required></td>
                </tr>
                <tr>
                    <td><label for="jk">Jenis Kelamin *: </label></td>
                    <td><input type="radio" name="jns_kelamin" value="Laki-laki" required>  Laki-laki <br/>
                    <input type="radio" name="jns_kelamin" value="Perempuan">  Perempuan
                    </td>
                </tr>
                <tr>
                <td><label for="jurusan">Pilih Jurusan *</label></td>
                <td><select class="col-sm-4" name="jurusan" required>
                    <option value="" selected disabled>---Pilih---</option>
                    <option value="Sistem Informasi">Sistem Informasi</option>
                    <option value="Informatika">Informatika</option>
                </select></td>
                </tr>
                <tr>
                    <td><label for="angkatan">Tahun Pendaftaran *: </label></td>
                    <td><input class="col-sm-3" type="number" name="thn_pendaftaran" min="2002" max="2020" required></td>
                </tr>
                <tr>
                    <td><label for="semester">Semester (jika mahasiswa baru, masukan semester satu) *: </label></td>
                    <td><input class="col-sm-2" type="number" name="semester" min="1" max="12" required></td>
                </tr>
                <tr>
                    <td><label for="ipk">IPK Terakhir (jika tidak ada tidak perlu diisi) *: </label></td>
                    <td><input class="col-sm-2" type="text" name="ipk">
                    <p id="note">Format penulisan IPK menggunakan titik (Contoh : 3.90)</p></td>
                </tr>
                <tr>
                <td><label for="foto">Upload Foto *</label></td>
                <td><input class="col-sm-4" type="file" name="foto" required></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" name="submit" value="Daftar" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Hapus Data" class="btn btn-warning col-sm-2"></td>
                </tr>
            </table>
        </form>    

    </div>
</body>
</html>

<?php

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $jurusan = $_POST['jurusan'];
    $jenis_kelamin = $_POST['jns_kelamin'];
    $angkatan = $_POST['thn_pendaftaran'];
    $semester = $_POST['semester'];
    $ipk = $_POST['ipk'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    $ext = explode('.', $foto);
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $nama.'_mahasiswa_baru.'.$ekstensi_file;

    $cek_nim = mysqli_query($koneksi, "SELECT * FROM data_mahasiswa WHERE nim='$nim'");

    if(in_array($ekstensi_file, $ekstensi_upload) == TRUE){
        move_uploaded_file($file_tmp, '../admin/foto/mahasiswa/'.$nama_foto);

        $input = mysqli_query($koneksi, "INSERT INTO data_mahasiswa (nama, jns_kelamin, foto, jurusan, thn_pendaftaran, semester, ipk_terbaru) VALUES ('$nama', '$jenis_kelamin', '$nama_foto', '$jurusan', '$angkatan', $semester, '$ipk')");

        if(!$input){
            echo "<script>alert('Proses input gagal !');</script>";
        }
        else{
            echo "<script>alert('Data berhasil ditambahkan');</script>";
            echo "<script>location='../index.php';</script> ";
            echo "<br/>";
        }

    }
    else{
        echo "<script>alert('Jenis file yang anda upload tidak didukung');</script>";
    }
}

?>