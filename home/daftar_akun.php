<?php
    include 'head.php';
?>

<body>
        <div class="wrapper login">
            <img src="../assets/img/logo_unas.png" alt="logo unas" width="80px" height="90px">
            <h3 class="judul-login">DAFTAR AKUN</h3>
            <div>
                <form method="POST" action="daftar_akun.php" class="form-login" enctype="multipart/form-data">
                    <div>
                        <label class="col-sm-4">Nama Lengkap</label>
                        <input class="col-sm-5" type="text" name="nama" required="required">
                    </div>
                    <br>
                    <div>
                        <label class="col-sm-4">Username :</label>
                        <input class="col-sm-5" type="text" name="username" required="required">
                    </div>
                    <br>
                    <div>
                        <label class="col-sm-4">Password :</label>
                        <input class="col-sm-5" type="password" name="password" required="required">
                    </div>
                    <br>
                    <div>
                        <label class="col-sm-4">Ulangi Password :</label>
                        <input class="col-sm-5" type="password" name="ulang_password" required="required">
                    </div>
                    <br>
                    <div>
                        <label class="col-sm-4">Hak Akses :</label>
                        <select class="col-sm-5" name="hak_akses">
                            <option value="admin">Admin</option>
                            <option value="dosen">Dosen</option>
                        </select>
                    </div>
                    <br>
                    <div>
                        <label class="col-sm-4">Foto Avatar / Akun :</label>
                        <input class="col-sm-5" type="file" name="foto" required>
                    </div>
                    <br>

                    <input type="submit" class="btn btn-success" name="daftar" value="Daftarkan">
                    <input type="reset" class="btn btn-danger" name="reset" value="Reset Data">
                    <br>
                </form>
            </div>
        </div>
    </body>
</html>

<?php
include '../connect.php';

if(isset($_POST['daftar'])){
    $nama = $_POST['nama'];
    $user = $_POST['username'];
    $password = md5($_POST['password']);
    $ulang = md5($_POST['ulang_password']);
    $hak = $_POST['hak_akses'];
    $foto = $_FILES['foto']['name'];
    $tmp_file = $_FILES['foto']['tmp_name'];
    $ext = explode('.', $foto);
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $user.'.'.$ekstensi_file;

    $cek_user = mysqli_query($koneksi, "SELECT * FROM data_akun WHERE username='$user'");
    $cek = mysqli_num_rows($cek_user);

    if($cek > 0){
        echo "<script>alert('Username sudah terdaftar, silahkan gunakan username yang lain');</script>";        
    }
    else{
        if (in_array($ekstensi_file, $ekstensi_upload) == TRUE) {
            move_uploaded_file($tmp_file, '../admin/avatar/'.$nama_foto);
            if($password==$ulang){
                $daftar = mysqli_query($koneksi, "INSERT INTO data_akun(nama, username, password, hak_akses, foto) VALUE('$nama', '$user', '$password', '$hak', '$nama_foto')");
                    if ($daftar) {
                        echo "<script>alert('Data berhasil dibuat, silahkan login');</script>";
                        echo "<script>location='login.php';</script>";        
                    }
                    else{
                        echo "<script>alert('Data gagal dibuat, silahkan coba lagi');</script>";
                    }
            }
            else{
                echo "<script>alert('Password yang dimasukan berbeda !!');</script>";
            }    
        }
        else{
            echo "<script>alert('Foto harus memiliki format file .jpg atau .png');</script>";            
        }
    }

}

?>