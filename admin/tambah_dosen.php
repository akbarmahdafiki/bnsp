<div class="wrapper">
    <center>
    <h2>Pendaftaran Dosen</h2><br/>
    </center>
    <form method="POST" action="index.php?page=tambah_dosen" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
                <td><label for="nama">Masukan Nama *: </label></td>
                <td><input class="col-sm-4" type="text" name="nama" required></td>
            </tr>
            <tr>
                <td><label for="no_induk">Masukan No. Induk *: </label></td>
                <td><input class="col-sm-4" type="text" name="no_induk" required></td>
            </tr>
            <tr>
            <td><label for="pendidikan_terakhir">Pendidikan Terakhir *</label></td>
            <td><select class="col-sm-4" name="pendidikan_terakhir" required>
                <option value="" selected disabled>---Pilih---</option>
                <option value="S1">S1</option>
                <option value="S2">S2</option>
                <option value="S3">S3</option>
            </select></td>
            </tr>
            <tr>
                <td><label for="foto">Upload Foto *</label></td>
                <td><input class="col-sm-4" type="file" name="foto"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Masukan Data" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Hapus Data" class="btn btn-warning col-sm-2"></td>
            </tr>
        </table>
    </form>    

</div>

<?php
include '../connect.php';

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $no_induk = $_POST['no_induk'];
    $pend = $_POST['pendidikan_terakhir'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    // Memecah variabel foto
    $ext = explode('.', $foto);
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $nama.'_'.$no_induk.'.'.$ekstensi_file;

    $cek_nim = mysqli_query($koneksi, "SELECT * FROM data_dosen WHERE no_induk='$no_induk'");

    // Mengecek nim yang sama
    if(mysqli_num_rows($cek_nim) > 0){
        echo "<script>alert('No. Induk Sudah Terdaftar, Mohon Cek Kembali ...');</script>";
    }
    else{
        if(in_array($ekstensi_file, $ekstensi_upload) == TRUE){
            move_uploaded_file($file_tmp, 'foto/dosen/'.$nama_foto);
            $input = mysqli_query($koneksi, "INSERT INTO data_dosen (nama, no_induk, pendidikan_terakhir, foto) VALUES ('$nama', '$no_induk', '$pend', '$nama_foto')");

            if(!$input){
                echo "<script>alert('Proses input gagal !');</script>";
            }
            else{
                echo "<script>alert('Data berhasil ditambahkan');</script>";
                echo "<script>location='index.php?page=data_dosen';</script> ";
                echo "<br/>";
            }
        }
        else{
            echo "<script>alert('Jenis file yang anda upload tidak didukung');</script>";
        }
    }
    }

?>