<?php
    include '../connect.php';

    $id = $_GET['id'];
    $tampil = mysqli_query($koneksi, "SELECT * FROM data_mahasiswa WHERE no_id=$id");
    $tampil_dosen = mysqli_query($koneksi, "SELECT nama FROM data_dosen ORDER BY 'no_id'");

    $data = mysqli_fetch_array($tampil);
?>

<div class="wrapper">
    <center>
    <h2>Edit Mahasiswa</h2><br/>
    </center>
    <form method="POST" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
            <td colspan="2">
            <?php
                if ($data['foto'] == NULL) {
                    echo "<img src='avatar/avatar_default.png' width='200' height='200'><br/><br/>";
                }
                else {
                    echo "<img src='foto/mahasiswa/".$data['foto']."' width='177' height='236'><br/><br/>
                    <a href='foto/mahasiswa/".$data['foto']."' target='_blank' class='btn btn-info'>Lihat Gambar</a>";
                }
            ?>
            </td>
            </tr>
            <tr>
                <td><label for="nama">Masukan Nama *: </label></td>
                <td><input class="col-sm-4" type="text" name="nama" value="<?php echo $data['nama']; ?>" required></td>
            </tr>
            <tr>
                <td><label for="nim">Masukan NIM *: </label></td>
                <td><input class="col-sm-4" type="text" name="nim" value="<?php echo $data['nim']; ?>" required></td>
            </tr>
            <tr>
                <td><label for="jk">Jenis Kelamin *: </label></td>
                <td><input type="radio" name="jns_kelamin" <?php if($data['jns_kelamin'] == 'Laki-laki'){ echo 'checked';} ?> value="Laki-laki" required>  Laki-laki <br/>
                <input type="radio" name="jns_kelamin" <?php if($data['jns_kelamin'] == 'Perempuan'){ echo 'checked';} ?> value="Perempuan">  Perempuan
                </td>
            </tr>
            <tr>
            <td><label for="jurusan">Pilih Jurusan *</label></td>
            <td><select class="col-sm-4" name="jurusan" required>
                <option value="" selected disabled>---Pilih---</option>
                <option value="Sistem Informasi" <?php if($data['jurusan'] == 'Sistem Informasi'){ echo 'selected';} ?>>Sistem Informasi</option>
                <option value="Informatika" <?php if($data['jurusan'] == 'Informatika'){ echo 'selected';} ?>>Informatika</option>
            </select></td>
            <tr>
                <td><label for="angkatan">Tahun Pendaftaran *: </label></td>
                <td><input class="col-sm-3" type="number" name="thn_pendaftaran" value="<?php echo $data['thn_pendaftaran']; ?>" min="2002" max="2020" required></td>
            </tr>
            <tr>
                <td><label for="semester">Semester *: </label></td>
                <td><input class="col-sm-2" type="number" name="semester" value="<?php echo $data['semester']; ?>" min="1" max="12" required></td>
            </tr>
            <tr>
                <td><label for="ipk">IPK Terbaru *: </label></td>
                <td><input class="col-sm-2" type="text" name="ipk" value="<?php echo $data['ipk_terbaru']; ?>" required>
                <p id="note">Format penulisan IPK menggunakan titik (Contoh : 3.90)</p></td>
            </tr>
            <tr>
                <td><label for="dosen">Dosen Pembimbing *: </label></td>
                <td><select class="col-sm-4" name="dosen" required>
                    <option value="" disabled selected>----Pilih----</option>
                    <?php
                    if(mysqli_num_rows($tampil_dosen) <= 0){
                        echo "<option value='belum ada'>Belum Ada Dosen yang Terdaftar</option>";
                    }
                    else{
                        while($data_tampil = mysqli_fetch_array($tampil_dosen)){    
                            echo "<option value='".$data_tampil['nama']."'>".$data_tampil['nama']."</option>";
                        }
                    }
                    ?>
                </select></td>
            </tr>
            </tr>
            <tr>
            <td><label for="foto">Upload Foto *</label></td>
            <td><input class="col-sm-4" type="file" name="foto"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Update Data" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Reset Data" class="btn btn-warning col-sm-2"></td>
            </tr>
        </table>
    </form>    

</div>

<?php

include '../connect.php';

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $nim = $_POST['nim'];
    $jurusan = $_POST['jurusan'];
    $jenis_kelamin = $_POST['jns_kelamin'];
    $angkatan = $_POST['thn_pendaftaran'];
    $semester = $_POST['semester'];
    $ipk = $_POST['ipk'];
    $dosen = $_POST['dosen'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    // Memecah variabel $foto menjadi array
    $ext = explode('.', $foto);
    // Membuat font menjadi lowercase
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $nama.'_'.$nim.'.'.$ekstensi_file;

    $cek_nim = mysqli_query($koneksi, "SELECT * FROM data_mahasiswa WHERE nim='$nim'");

    if(mysqli_num_rows($cek_nim) > 0 && $nim != $data['nim']){
        echo "<script>alert('NIM Sudah Terdaftar, Mohon Cek Kembali ...');</script>";
    }
    else{
        if(in_array($ekstensi_file, $ekstensi_upload) == TRUE || $file_tmp == NULL){
            if ($ipk <= 0 || $ipk > 4) {
                echo "<script>alert('Proses update gagal, perhatikan nilai ipk yang anda masukan !');</script>";
            }
            else{
                if ($foto == NULL) {
                    $input = mysqli_query($koneksi, "UPDATE data_mahasiswa SET nama='$nama', nim='$nim', jns_kelamin='$jenis_kelamin', jurusan='$jurusan', thn_pendaftaran='$angkatan', semester='$semester', ipk_terbaru = '$ipk', dosen_pembimbing='$dosen' WHERE no_id=$id");
                }
                else{
                    move_uploaded_file($file_tmp, 'foto/mahasiswa/'.$nama_foto);
                    $input = mysqli_query($koneksi, "UPDATE data_mahasiswa SET nama='$nama', nim='$nim', jns_kelamin='$jenis_kelamin', foto='$nama_foto', jurusan='$jurusan', thn_pendaftaran='$angkatan', semester='$semester', ipk_terbaru = '$ipk', dosen_pembimbing='$dosen' WHERE no_id=$id");
                }

                // Jika proses input error
                if(!$input){
                    echo "<script>alert('Proses update gagal, silahkan perhatikan form yang anda isi !');</script>";
                }
                else{
                    echo "<script>alert('Data Berhasil Diupdate');</script>";
                    echo "<script>location='index.php?page=data_mahasiswa';</script> ";
                    echo "<br/>";
                }
            }

        }
        // Jika format file yang diupload bukan png atau jpg
        else{
            echo "<script>alert('Jenis file yang anda upload tidak didukung, silahkan upload file yang berformat .jpg atau .png');</script>";
        }
    
    }
}
?>