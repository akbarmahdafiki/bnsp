<div class="wrapper">
    <center><h2>Dosen yang Terdaftar</h2></center>
<div>
<!-- Form Pencarian -->
    <form action="index.php?page=data_dosen" method="POST">
    <table class="table table-borderless">
        <tr>
            <td><label class="float-right" for="cari">Cari </label></td>
            <td><input class="col-sm-4" type="text" name="search"></td>
        </tr>
        <tr>
            <td><label class="float-right" for="cari_kolom">Pada </label></td>
            <td><select class="col-sm-4" name="nama_kolom" required>
                <option value="" selected disabled>---Pilih---</option>
                <option value="nama">Nama</option>
                <option value="no_induk">No. Induk</option>
                <option value="pendidikan_terakhir">Pendidikan Terakhir</option>
            </select></td>
        </tr>
        <td colspan="2">
            <input class="btn btn-success col-sm-8" type="submit" name="cari" value="Cari">
        </td>
    </table>
    </form>
</div>
<!-- Tabel Dosen -->
<div class="table-responsive">
    <table width="100%" class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th width="10px">No.</th>
                <th width="30px">Foto</th>
                <th width="200px">Nama</th>
                <th width="200px">No. Induk</th>
                <th width="50x">Pendidikan Terakhir</th>
                <th width="200px">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include '../connect.php';

            $tampil = mysqli_query($koneksi, "SELECT * FROM data_dosen ORDER BY 'no_id' DESC");

            if(isset($_POST['cari'])){

                $cari = $_POST['search'];
                $nama_kolom = $_POST['nama_kolom'];
                
                //Data input cari dan nama kolom tidak boleh kosong
                if($cari == '' || $nama_kolom == ''){
                    echo "<script>alert('Data Tidak Boleh Kosong !!');</script>";                    
                }
                else{
                    $tampil = mysqli_query($koneksi, "SELECT * FROM data_dosen WHERE $nama_kolom LIKE '%{$cari}%'");
                }
            }

            //Jika data kosong, maka ...
            if(mysqli_num_rows($tampil) <= 0){
                echo "<tr>";
                    echo "<td colspan='6'><p>Data Kosong</p></td>";
                echo "</tr>";
            }
            else{
                $no = 1;
                // Mengubah tabel mysql menjadi array
                while($data = mysqli_fetch_array($tampil)){
                echo "<tr>";
                    echo "<td>".$no."</td>";
                    if ($data['foto'] == NULL) {
                        echo "<td><img src='avatar/avatar_default.png' width='100' height='100'></td>";
                    }
                    else {
                        echo "<td><img src='foto/dosen/".$data['foto']."' width='88.5' height='118'></td>";
                    }
                    echo "<td>".$data['nama']."</td>";
                    echo "<td>".$data['no_induk']."</td>";
                    echo "<td>".$data['pendidikan_terakhir']."</td>";
                    echo "<td><a href='index.php?page=edit_dosen&id=".$data['no_id']."' class='btn btn-warning'>Edit&nbsp<i class='fa fa-pencil-square-o'></i></a>&nbsp";
                    // Konfirmasi menghapus data
                    echo "<a "?> onClick="return confirm('Apakah anda yakin ingin menghapus data <?php echo $data['nama'];?> ?')" <?php echo "href='index.php?page=hapus_dosen&id=".$data['no_id']."' class='btn btn-danger'>Hapus&nbsp<i class='fa fa-times'></i></a></td>";
                    $no++;
                echo "</tr>";
                }
            }
            ?>
        </tbody>
    </table>
    <?php
    // Menampilkan jumlah dosen yang terdaftar
    echo "<p>Jumlah dosen terdaftar : " .mysqli_num_rows($tampil). "</p>";
    ?>
</div>

</div>