<?php
    include '../connect.php';

    function tampil_data($kolom, $tabel){
        include '../connect.php';
        $tampil = mysqli_query($koneksi, "SELECT $kolom FROM $tabel");
        echo mysqli_num_rows($tampil);
    }

?>

<div class="wrapper">
    <center>
        <h1>Selamat Datang di Website Universitas Nasional</h1>
        <p>Silahkan pilih menu disamping untuk mengakses data mahasiswa dan dosen</p>
    </center>
    <div class="row justify-content-center padding-card">
        <div class="card padding-card margin-card text-center col-sm-3 bg-danger text-white">
            <p class="card-title">Dosen yang terdaftar</p>
            <h2 class="card-title"><?php tampil_data('*', 'data_dosen'); ?></h2>
            <a href ="index.php?page=data_dosen" class="card-body">Lihat selengkapnya&nbsp&nbsp<i class="fa fa-arrow-circle-right"></i></a>
        </div>
        <div class="card padding-card margin-card text-center col-sm-3 bg-primary text-white">
            <p class="card-title">Mahasiswa yang terdaftar</p>
            <h2><?php tampil_data('*', 'data_mahasiswa'); ?></h2>
            <a href ="index.php?page=data_mahasiswa" class="card-body">Lihat selengkapnya&nbsp&nbsp<i class="fa fa-arrow-circle-right"></i></a>
            <p></p>
        </div>
    </div>
</div>