<?php
    include '../connect.php';

    $id = $_GET['id'];
    $tampil = mysqli_query($koneksi, "SELECT * FROM data_dosen WHERE no_id=$id");

    $data = mysqli_fetch_array($tampil);
?>

<div class="wrapper">
    <center>
    <h2>Edit Dosen</h2><br/>
    </center>
    <form method="POST" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
            <td colspan="2">
            <?php
                if ($data['foto'] == NULL) {
                    echo "<img src='avatar/avatar_default.png' width='200' height='200'><br/><br/>";
                }
                else {
                    echo "<img src='foto/dosen/".$data['foto']."' width='177' height='236'><br/><br/>
                    <a href='foto/dosen/".$data['foto']."' target='_blank' class='btn btn-info'>Lihat Gambar</a>";
                }
            ?>
            </td>
            </tr>
            <tr>
                <td><label for="nama">Masukan Nama *: </label></td>
                <td><input class="col-sm-4" type="text" name="nama" value="<?php echo $data['nama']; ?>" required></td>
            </tr>
            <tr>
                <td><label for="no_induk">Masukan No. Induk *: </label></td>
                <td><input class="col-sm-4" type="text" name="no_induk" value="<?php echo $data['no_induk']; ?>" required></td>
            </tr>
            <tr>
            <td><label for="pendidikan_terakhir">Pendidikan Terakhir *</label></td>
            <td><select class="col-sm-4" name="pendidikan_terakhir" value="<?php echo $data['pendidikan_terakhir']; ?>" required>
                <option value="" selected disabled>---Pilih---</option>
                <option value="S1" <?php if($data['pendidikan_terakhir'] == 'S1'){ echo 'selected';} ?> >S1</option>
                <option value="S2" <?php if($data['pendidikan_terakhir'] == 'S2'){ echo 'selected';} ?> >S2</option>
                <option value="S3" <?php if($data['pendidikan_terakhir'] == 'S3'){ echo 'selected';} ?> >S3</option>
            </select></td>
            </tr>
            <tr>
                <td><label for="foto">Upload Foto *</label></td>
                <td><input class="col-sm-4" type="file" name="foto"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="subsmit" value="Update Data" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Reset Data" class="btn btn-warning col-sm-2"></td>
            </tr>
        </table>
    </form>    

</div>

<?php

include '../connect.php';

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $no_induk = $_POST['no_induk'];
    $pend = $_POST['pendidikan_terakhir'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    // Memecah variabel $foto menjadi array
    $ext = explode('.', $foto);
    // Membuat font menjadi lowercase
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $nama.'_'.$no_induk.'.'.$ekstensi_file;

    $cek_induk = mysqli_query($koneksi, "SELECT * FROM data_dosen WHERE no_induk='$no_induk'");

    if(mysqli_num_rows($cek_induk) > 0 && $no_induk != $data['no_induk']){
        echo "<script>alert('No. Induk Sudah Terdaftar, Mohon Cek Kembali ...');</script>";
    }
    else{
        if(in_array($ekstensi_file, $ekstensi_upload) == TRUE || $file_tmp == NULL){
            
            //Jika foto tidak diupload
            if($foto == NULL){
                $input = mysqli_query($koneksi, "UPDATE data_dosen SET nama='$nama', no_induk='$no_induk', pendidikan_terakhir='$pend' WHERE no_id=$id");        
            }
            else{
                // Memindahkan file foto ke direktori foto
                move_uploaded_file($file_tmp, 'foto/dosen/'.$nama_foto);
                $input = mysqli_query($koneksi, "UPDATE data_dosen SET nama='$nama', no_induk='$no_induk', pendidikan_terakhir='$pend', foto='$nama_foto' WHERE no_id=$id");
            }
            // Jika proses input error
            if(!$input){
                echo "<script>alert('Proses Update Gagal !');</script>";
            }
            else{
                echo "<script>alert('Data Berhasil Diupdate');</script>";
                echo "<script>location='index.php?page=data_dosen';</script> ";
                echo "<br/>";
            }
        }
        // Jika format file yang diupload bukan png atau jpg
        else{
            echo "<script>alert('Jenis file yang anda upload tidak didukung, silahkan upload file yang berformat .jpg atau .png');</script>";
        }
    
    }
}
?>