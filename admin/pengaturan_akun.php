<?php
    include '../connect.php';
    
    $tampil = mysqli_query($koneksi, "SELECT * FROM data_akun WHERE username='$user'");

    $data = mysqli_fetch_array($tampil);
?>

<div class="wrapper">
    <center>
    <h2>Edit Akun</h2><br/>
    </center>
    <form method="POST" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
            <td colspan="2">
            <?php
                echo "<img src='avatar/".$data['foto']. "' width='177' height='236'><br/><br/>
                <a href='avatar/".$data['foto']."' target='_blank' class='btn btn-info'>Lihat Gambar</a>";
            ?>
            </td>
            </tr>
            <tr>
                <td><label for="nama">Nama Lengkap *: </label></td>
                <td><input class="col-sm-4" type="text" name="nama" value="<?php echo $data['nama']; ?>" required></td>
            </tr>
            <tr>
                <td><label for="username">Username *: </label></td>
                <td><input class="col-sm-4" type="text" name="username" value="<?php echo $data['username']; ?>" required></td>
            </tr>
            <tr>
                <td><label for="password">Password Baru *: </label></td>
                <td><input class="col-sm-4" type="password" name="password" required></td>
            </tr>
            <tr>
                <td><label for="ulang_password">Ulangi Password *: </label></td>
                <td><input class="col-sm-4" type="password" name="ulang_password" required></td>
            </tr>
            <tr>
                <td><label for="akses">Hak Akses *: </label></td>
                <td><select class="col-sm-4" name="hak_akses" required>
                    <option value="" disabled>----Pilih----</option>
                    <option value="admin" <?php if($data['hak_akses'] == 'admin'){echo 'selected';} ?>>Admin</option>
                    <option value="dosen" <?php if($data['hak_akses'] == 'dosen'){echo 'selected';} ?>>Dosen</option>
                </select></td>
            </tr>
            <tr>
                <td><label for="foto">Upload Foto *</label></td>
                <td><input class="col-sm-4" type="file" name="foto"></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Masukan Data" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Data Default" class="btn btn-warning col-sm-2"></td>
            </tr>
        </table>
    </form>    

</div>

<?php

include '../connect.php';

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $ulang_password = $_POST['ulang_password'];
    $hak = $_POST['hak_akses'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    // Memecah variabel $foto
    $ext = explode('.', $foto);
    // Membuat string menjadi lowercase
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $username.'.'.$ekstensi_file;

    $cek_user = mysqli_query($koneksi, "SELECT * FROM data_akun WHERE username='$username'");

    if(mysqli_num_rows($cek_user) > 0 && $username != $data['username']){
        echo "<script>alert('NIM Sudah Terdaftar, Mohon Cek Kembali ...');</script>";
    }
    else{
        if(in_array($ekstensi_file, $ekstensi_upload) == TRUE || $file_tmp == NULL){
            if($password == $ulang_password){
                // Jika foto tidak diupload
                if($foto == NULL){
                    $input = mysqli_query($koneksi, "UPDATE data_akun SET nama='$nama', username='$username', password='$password', hak_akses='$hak' WHERE username='$user'");
                }
                else{
                    move_uploaded_file($file_tmp, 'avatar/'.$nama_foto);
                    $input = mysqli_query($koneksi, "UPDATE data_akun SET nama='$nama', username='$username', password='$password', hak_akses='$hak', foto='$nama_foto' WHERE username='$user'");
                }
                
                if(!$input){
                    echo "<script>alert('Proses Input Gagal !');</script>";
                }
                else{
                    $_SESSION['username'] = $username;
                    echo "<script>alert('Data Berhasil Diupdate');</script>";
                    echo "<script>location='index.php?page=data_mahasiswa';</script> ";
                    echo "<br/>";
                }
            }
        }
        else{
            echo "<script>alert('Jenis file yang anda upload tidak didukung, silahkan upload file yang berformat .jpg atau .png');</script>";
        }
    
    }
}
?>