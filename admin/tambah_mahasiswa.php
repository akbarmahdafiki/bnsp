<?php
    include '../connect.php';                
    $tampil_dosen = mysqli_query($koneksi, "SELECT nama FROM data_dosen ORDER BY 'no_id'");
    
?>

<div class="wrapper">
    <center>
    <h2>Pendaftaran Mahasiswa</h2><br/>
    </center>
    <form method="POST" action="index.php?page=tambah_mahasiswa" enctype="multipart/form-data">
        <table class="table table-striped">
            <tr>
                <td><label for="nama">Masukan Nama *: </label></td>
                <td><input class="col-sm-4" type="text" name="nama" required></td>
            </tr>
            <tr>
                <td><label for="nim">Masukan NIM *: </label></td>
                <td><input class="col-sm-4" type="text" name="nim" required></td>
            </tr>
            <tr>
                <td><label for="jk">Jenis Kelamin *: </label></td>
                <td><input type="radio" name="jns_kelamin" value="Laki-laki" required>  Laki-laki <br/>
                <input type="radio" name="jns_kelamin" value="Perempuan">  Perempuan
                </td>
            </tr>
            <tr>
            <td><label for="jurusan">Pilih Jurusan *</label></td>
            <td><select class="col-sm-4" name="jurusan" required>
                <option value="" selected disabled>---Pilih---</option>
                <option value="Sistem Informasi">Sistem Informasi</option>
                <option value="Informatika">Informatika</option>
            </select></td>
            </tr>
            <tr>
                <td><label for="angkatan">Tahun Pendaftaran *: </label></td>
                <td><input class="col-sm-3" type="number" name="thn_pendaftaran" min="2002" max="2020" required></td>
            </tr>
            <tr>
                <td><label for="semester">Semester *: </label></td>
                <td><input class="col-sm-2" type="number" name="semester" min="1" max="12" required></td>
            </tr>
            <tr>
                <td><label for="ipk">IPK Terbaru *: </label></td>
                <td><input class="col-sm-2" type="text" name="ipk" required>
                <p id="note">Format penulisan IPK menggunakan titik (Contoh : 3.90)</p></td>
            </tr>
            <tr>
                <td><label for="dosen">Dosen Pembimbing *: </label></td>
                <td><select class="col-sm-4" name="dosen" required>
                    <option value="" disabled selected>----Pilih----</option>
                    <?php
                    if(mysqli_num_rows($tampil_dosen) <= 0){
                        echo "<option value='belum ada'>Belum Ada Dosen yang Terdaftar</option>";
                    }
                    else{
                        while($data = mysqli_fetch_array($tampil_dosen)){    
                            echo "<option value='".$data['nama']."'>".$data['nama']."</option>";
                        }
                    }
                    ?>
                </select></td>
            </tr>
            <tr>
            <td><label for="foto">Upload Foto *</label></td>
            <td><input class="col-sm-4" type="file" name="foto" required></td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" name="submit" value="Masukan Data" class="btn btn-success col-sm-2">&nbsp&nbsp<input type="reset" name="reset" value="Hapus Data" class="btn btn-warning col-sm-2"></td>
            </tr>
        </table>
    </form>    

</div>

<?php

if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $nim = $_POST['nim'];
    $jurusan = $_POST['jurusan'];
    $jenis_kelamin = $_POST['jns_kelamin'];
    $angkatan = $_POST['thn_pendaftaran'];
    $semester = $_POST['semester'];
    $ipk = $_POST['ipk'];
    $dosen = $_POST['dosen'];
    $foto = $_FILES['foto']['name'];
    $file_tmp = $_FILES['foto']['tmp_name'];
    // Memecah variabel $foto
    $ext = explode('.', $foto);
    // Membuat string menjadi lowercase
    $ekstensi_file = strtolower(end($ext));
    $ekstensi_upload = array('jpg', 'png');
    $nama_foto = $nama.'_'.$nim.'.'.$ekstensi_file;

    $cek_nim = mysqli_query($koneksi, "SELECT * FROM data_mahasiswa WHERE nim='$nim'");

    if(mysqli_num_rows($cek_nim) > 0){
        echo "<script>alert('NIM Sudah Terdaftar, Mohon Cek Kembali ...');</script>";
    }
    else{
        if(in_array($ekstensi_file, $ekstensi_upload) == TRUE){
            move_uploaded_file($file_tmp, 'foto/mahasiswa/'.$nama_foto);
            if ($ipk <= 0 || $ipk > 4) {
                echo "<script>alert('Gagal input data, perhatikan kembali penulisan nilai IPK !');</script>";                
            }
            else{
                $input = mysqli_query($koneksi, "INSERT INTO data_mahasiswa (nama, nim, jns_kelamin, foto, jurusan, thn_pendaftaran, semester, ipk_terbaru, dosen_pembimbing) VALUES ('$nama', '$nim', '$jenis_kelamin', '$nama_foto', '$jurusan', '$angkatan', $semester, '$ipk', '$dosen')");

                if(!$input){
                    echo "<script>alert('Proses input gagal !');</script>";
                }
                else{
                    echo "<script>alert('Data berhasil ditambahkan');</script>";
                    echo "<script>location='index.php?page=data_mahasiswa';</script> ";
                    echo "<br/>";
                }
            }

        }
        else{
            echo "<script>alert('Jenis file yang anda upload tidak didukung');</script>";
        }
    }
}

?>