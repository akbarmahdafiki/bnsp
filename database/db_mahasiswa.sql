-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2021 at 05:16 AM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_mahasiswa`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_akun`
--

CREATE TABLE `data_akun` (
  `no_id` int(30) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL,
  `foto` text NOT NULL,
  `hak_akses` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_akun`
--

INSERT INTO `data_akun` (`no_id`, `nama`, `username`, `password`, `foto`, `hak_akses`) VALUES
(1, 'satu', 'satu1', '202cb962ac59075b964b07152d234b70', 'satu1.png', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `data_dosen`
--

CREATE TABLE `data_dosen` (
  `no_id` int(40) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `no_induk` varchar(30) NOT NULL,
  `pendidikan_terakhir` varchar(10) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_dosen`
--

INSERT INTO `data_dosen` (`no_id`, `nama`, `no_induk`, `pendidikan_terakhir`, `foto`) VALUES
(1, 'Yunan Fauzi Wijaya, S.Kom., MMSI', '01111', 'S2', 'Yunan Fauzi Wijaya, S.Kom., MMSI_01111.png'),
(2, 'Dr. Fauziah, MMSI', '01112', 'S3', 'Dr. Fauziah, MMSI_01112.png'),
(3, 'Agus Iskandar, S.Kom., M.Kom', '01113', 'S2', 'Agus Iskandar, S.Kom., M.Kom_01113.png'),
(4, 'Arie Gunawan, S.Kom., MMSI.', '01114', 'S2', 'Arie Gunawan, S.Kom., MMSI._01114.png'),
(5, 'Raden Muhammad Firzatullah, S.Kom, M.kom', '01115', 'S2', 'Raden Muhammad Firzatullah, S.Kom, M.kom_01115.png'),
(6, 'Andrianingsih, S.Kom., MMSI.', '01116', 'S2', 'Andrianingsih, S.Kom., MMSI._01116.png');

-- --------------------------------------------------------

--
-- Table structure for table `data_mahasiswa`
--

CREATE TABLE `data_mahasiswa` (
  `no_id` int(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `nim` varchar(50) NOT NULL,
  `jns_kelamin` varchar(20) NOT NULL,
  `foto` text NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `thn_pendaftaran` varchar(20) NOT NULL,
  `semester` int(20) NOT NULL,
  `ipk_terbaru` varchar(20) NOT NULL,
  `dosen_pembimbing` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_mahasiswa`
--

INSERT INTO `data_mahasiswa` (`no_id`, `nama`, `nim`, `jns_kelamin`, `foto`, `jurusan`, `thn_pendaftaran`, `semester`, `ipk_terbaru`, `dosen_pembimbing`) VALUES
(1, 'Rizki Akbar Mahdafiki', '1831127076450071', 'Laki-laki', 'Rizki Akbar Mahdafiki_1831127076450071.png', 'Informatika', '2018', 5, '3.91', 'Agus Iskandar, S.Kom., M.Kom'),
(2, 'Muhammad Ridwan', '173112700650098', 'Laki-laki', 'Muhammad Ridwan_173119287340511.jpg', 'Sistem Informasi', '2017', 7, '3.92', 'Dr. Fauziah, MMSI'),
(3, 'Syifa Faradilla F.', '173112700650060', 'Perempuan', 'Syifa Faradilla F._173112704854471.jpg', 'Sistem Informasi', '2017', 7, '3.99', 'Andrianingsih, S.Kom., MMSI.'),
(4, 'Anggita Putri Maharani', '183112700650175', 'Perempuan', 'Anggita Putri Maharani_183112700650175.jpg', 'Sistem Informasi', '2018', 5, '3.98', 'Andrianingsih, S.Kom., MMSI.'),
(5, 'Danang Aji Pangestuu', '1731112706450221', 'Laki-laki', 'Danang Aji Pangestuu_1731112706450221.jpg', 'Informatika', '2018', 7, '3.98', 'Yunan Fauzi Wijaya, S.Kom., MMSI');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_akun`
--
ALTER TABLE `data_akun`
  ADD PRIMARY KEY (`no_id`);

--
-- Indexes for table `data_dosen`
--
ALTER TABLE `data_dosen`
  ADD PRIMARY KEY (`no_id`);

--
-- Indexes for table `data_mahasiswa`
--
ALTER TABLE `data_mahasiswa`
  ADD PRIMARY KEY (`no_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_akun`
--
ALTER TABLE `data_akun`
  MODIFY `no_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `data_dosen`
--
ALTER TABLE `data_dosen`
  MODIFY `no_id` int(40) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `data_mahasiswa`
--
ALTER TABLE `data_mahasiswa`
  MODIFY `no_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
