<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Website Universitas Nasional</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<link rel="icon" href="assets/img/logo_unas.png">
<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" />
<link href="assets/css/default.css" rel="stylesheet" type="text/css" media="all" />
<link href="assets/css/fonts.css" rel="stylesheet" type="text/css" media="all" />


</head>
<body>
<div id="wrapper">
	<div id="header" class="container">
		<div id="logo">
			<h1><img src="assets/img/logo_unas.png" alt="logo_unas" width="50" height="55">&nbsp<a href="#">Universitas Nasional</a></h1>
		</div>
		<div id="menu">
			<ul>
				<li class="current_page_item"><a href="#" accesskey="1" title="">Homepage</a></li>
				<li><a href="home/login.php" accesskey="2" title="">Login</a></li>
				<li><a href="home/daftar_mahasiswa.php" accesskey="3" title="">Pendaftaran Mahasiswa</a></li>
			</ul>
		</div>
	</div>
	<div id="banner">&nbsp;</div>
	<div id="featured">
		<div class="container">
			<div class="title">
				<h2>Selamat Datang Di</h2>
				<h3 class="byline">Website Universitas Nasional</h3><br/>
				<img src="assets/img/logo_unas.png" alt="logo_unas" width="120" height="140">
			</div>
			<p>Silahkan login agar dapat mengakses halaman admin</p>
		</div>
		<ul class="actions">
			<li><a href="home/login.php" class="button">Login</a></li>
		</ul>
	</div>
</div>
<div id="copyright" class="container">
	<p>&copy; Rizki Akbar Mahdafiki. All rights reserved. | Photos from <a href="http://unas.ac.id/">Webkuliah Universitas Nasional</a></p>
</div>
</body>
</html>
